#lang racket

;lab sheet 1 exercise 1

;a) Add to 9 then multiply by 2 to give 18
;b) Adds to 4 then test if these are equal to 3, which isnt
;c) takes first elemnent of the list, returning elmer
;d) Takes the tail of the list, i.e. fudd daffy duck
;e) tests if 1 = 2, isnt true so returns false and does not check the division by zero

(* 2 (+ 4 5))
(= 3 (+ 1 3))
(car '(elmer fudd daffy duck))
(cdr '(elmer fudd daffy duck))
(and (= 1 2) (= 10 (/ 1 0)))

-module(mymodule).
-export([sign/1]).

sign(N) when N > 0 -> 1;
sign(N) when N < 0 -> -1;
sign(_) -> 0.
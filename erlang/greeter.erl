-module(greeter).
-export([hello/1,map/2]).

%a
hello(N) -> io:format("Hello ~p!~n", [N]). 

%b

%4> F1 = fun(N) -> io:format("Hello ~p!~n", [N])end
%14> .
%#Fun<erl_eval.6.54118792>
%15> F1(paul).
%Hello paul!
%ok

map(F,[H|T]) -> [F(H)| map(F,T)];
map(_,[]) -> []. 
